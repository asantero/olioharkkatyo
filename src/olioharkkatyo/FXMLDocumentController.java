
package olioharkkatyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Antti
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private WebView karttaIkkuna;
    @FXML
    private ComboBox<SmartPostAutomat> comboBox;
    @FXML
    private Button addAutomatButton;
    @FXML
    private Button packetSendButton;
    @FXML
    private ComboBox<PacketClass> packetBox;
    @FXML
    private Button routeRemoveButton;
    @FXML
    private Button createPacketButton;
    @FXML
    private Button packetLogButton;
    @FXML
    private TextArea logTextArea;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Create some sample objects to put in packages on programstart
        ObjectTracker.getInstance().addObject(new PacketObject(100000,100,"Teekkari",false));
        ObjectTracker.getInstance().addObject(new PacketObject(27000,12,"Kori Olvi III",true));
        ObjectTracker.getInstance().addObject(new PacketObject(23000,50,"Sähkömoottori",true));
        ObjectTracker.getInstance().addObject(new PacketObject(5000,60,"Alasin",false));

        //Load the map
        karttaIkkuna.getEngine().load(getClass().getResource("index.html").toExternalForm());
        URL urli = null;
        //set the url where automatdata is located
        try {
            urli = new URL("http://smartpost.ee/fi_apt.xml");
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //reader for the datastream from url
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(urli.openStream()));
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }       
        String content = "";
        String line;
        //read every line and put in to one string
        try {
            while((line = br.readLine()) != null) {
                //System.out.println(line);
                content += line + "\n";
            }
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //create a new dataparserobject and pass all the xml data we read as a string
        AutomatDataParser pA = new AutomatDataParser(content);
        //set it to null for garbagecollection
        pA = null;
        //Loop through the automat- and packettrackerobjects and populate the comboboxes
        for(int i = 0; i < AutomatTracker.getInstance().size; i++){
             comboBox.getItems().add(AutomatTracker.autoArr.get(i));
        }
        for (int i = 0; i < PacketStorage.getInstance().StorageArr.size(); i++){
            packetBox.getItems().add(PacketStorage.getInstance().StorageArr.get(i));
        }
    }    

    @FXML
    private void placeAutomatOnMap(ActionEvent event) {
        //check if theres an automat selected in the combobox
        if(comboBox.getValue() != null){
            //get the automatobject
            SmartPostAutomat buff = comboBox.getValue();
            //run the script for placing automat on the map
            karttaIkkuna.getEngine().executeScript("document.goToLocation('" + buff.address + ", " + buff.code + " " + buff.city + "', '" + buff.office + " -- Avoinna: " + buff.availability + "', 'blue')");
        }
    }

    @FXML
    private void drawPacketRoute(ActionEvent event) {
        //check if theres an automat selected in the combobox
        if(packetBox.getValue() != null){
            //get the packetobject
            PacketClass buff = packetBox.getValue();
            //run the pathdrawing script and grab the distance
            Double travelDistance = (Double)karttaIkkuna.getEngine().executeScript("document.createPath(" + buff.coordList + ", '" +  buff.drawColor + "', " + buff.classType + ")");
            //get current date and time
            Date date = new Date();
            //set up a String to write to the log
            String log = date.toString() + " Lähetetty esine: " + buff.deliveryObject.name + ", Pakettiluokka: " + buff.classType + ", Lähtöpaikka: " + buff.originName + ", Määränpää: " + buff.destinationName + ", Kuljettu matka: " + travelDistance + "km\n";
            //append the string to the textArea located in the other tab of the main program window
            logTextArea.appendText(log);
            //add a log to logkeeperobject
            PacketLogKeeper.getInstance().addLogEntry(log);
        }
    }

    @FXML
    private void removeDrawnRoutes(ActionEvent event) {
        //run the script to removepaths
        karttaIkkuna.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void openPacketCreationWindow(ActionEvent event) {
        try {
            //open a new window for creating a new package
            Stage filename = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("pakettiFXML.fxml"));
            Scene scene = new Scene(page);
            filename.setScene(scene);
            filename.showAndWait();
            //check if a new package was created
            if(PackageCreationChecker.getInstance().getStatus()){
                //add a new package to designated combobox
                packetBox.getItems().add(PacketStorage.getInstance().StorageArr.get(PacketStorage.getInstance().size - 1));
            }
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    

}
