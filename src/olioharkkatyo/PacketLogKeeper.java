/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Antti
 */
//Object that keeps track of the log of the current session. Similar to AutomatTracker and objectTracker
public class PacketLogKeeper {
    protected static ArrayList<String> logArr;
    private static PacketLogKeeper instance = null;
    protected static int size;

    public PacketLogKeeper() {
        logArr = new ArrayList();
    }
    public static PacketLogKeeper getInstance() {
        if(instance == null) {
            instance = new PacketLogKeeper();
        }
        return instance;
    }
    public static void addLogEntry(String logLine){
        logArr.add(logLine);
        size++;
    }
}
