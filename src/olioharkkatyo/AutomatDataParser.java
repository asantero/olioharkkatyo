
package olioharkkatyo;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Antti
 */

public class AutomatDataParser {
    
    private Document doc;
    private HashMap<String, String> map;

    public HashMap<String, String> getMap() {
        return map;
    }
    
    //constructor for the dataparser
    public AutomatDataParser(String content) {
        try {
            //xml dataparser works here
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            //create a new hashmap for the data
            map = new HashMap();
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(AutomatDataParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void parseCurrentData() {
        //make a list of all place elements
        NodeList nodes = doc.getElementsByTagName("place");
        //run through all the elements and put the values on the map with key
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            map.put("code", getValue("code", e ));
            map.put("city", getValue("city", e));
            map.put("address", getValue("address", e));
            map.put("availability", getValue("availability", e));
            map.put("postoffice", getValue("postoffice", e));
            map.put("lat", getValue("lat", e));
            map.put("lng", getValue("lng", e));
            //on each loop iteration create a new postautomatobject and add it to the object that keeps track of them
            AutomatTracker.getInstance().addAutomat(new SmartPostAutomat(map));
        }
    
    
    }
    
    //method for getting the value of an subelement in a place element
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    
    }
    
    
    
}


