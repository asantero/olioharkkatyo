
package olioharkkatyo;

import java.util.HashMap;

/**
 *
 * @author Antti
 */

//The automatobject that contains all the data we parsed from the xml data as strings
public class SmartPostAutomat {
    protected String city,code,office,address,availability,lattidude,longitude;
    
    //constructor takes hashmap as parameter and sets variables based on it
    //using a hashmap might be pointless in this but i don't have time to try a more direct approach
    public SmartPostAutomat(HashMap map){
        city = (String)map.get("city");
        code = (String)map.get("code");
        office = (String)map.get("postoffice");
        address = (String)map.get("address");
        availability = (String)map.get("availability");
        lattidude = (String)map.get("lat");
        longitude = (String)map.get("lng");     
    }
    
    //familiar override of tostring method for combobox
    @Override
    public String toString() {
        return address + ", " + city + " " + code;
    }
}
