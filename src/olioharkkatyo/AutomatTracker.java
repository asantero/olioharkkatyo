
package olioharkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Antti
 */
public class AutomatTracker {
    protected static ArrayList<SmartPostAutomat> autoArr;
    private static AutomatTracker instance = null;
    protected int size;

    //constructor creates a new ArrayList that can contain postautomat objects and sets size integer to zero
    protected AutomatTracker() {
        autoArr = new ArrayList();
        size = 0;
    }
    //method takes in an automat object and adds it to the arraylist and increases size by one
    public void addAutomat(SmartPostAutomat automat){
        autoArr.add(automat);
        size++;
    }
    
    //getInstance method to avoid creating more than one of this object
   public static AutomatTracker getInstance() {
        if(instance == null) {
            instance = new AutomatTracker();
        }
        return instance;
    }
    
}
