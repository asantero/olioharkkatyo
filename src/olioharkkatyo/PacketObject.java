
package olioharkkatyo;


/**
 *
 * @author Antti
 */

//The object that is put into a packet
public class PacketObject {
    protected int size;
    protected boolean fragile;
    protected double mass;
    protected String name;
    
    public PacketObject(int Size, double Mass, String Name, boolean Fragile){
        size = Size;
        fragile = Fragile;
        mass = Mass;
        name = Name;
    }
    
    //override the toString method so combobox shows the objects name
    @Override
    public String toString() {
        return name;
    }
}
