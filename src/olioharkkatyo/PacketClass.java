
package olioharkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Antti
 */

//abstract packetclass that we can inherit from
abstract class PacketClass {
    protected int classType;
    protected String  drawColor, originName, destinationName;
    protected ArrayList<String> coordList;
    protected PacketObject deliveryObject;
    
    //overrides the default toString method so the combobox shows to the user what we want
    @Override
    public String toString() {
        return "Pakettiluokka" + classType + " - Esine: " + deliveryObject.name;
    }
}

//subclasses created with minor differences
class firstClass extends PacketClass{

        public firstClass(PacketObject object, SmartPostAutomat auto1, SmartPostAutomat auto2){
        classType = 1;
        deliveryObject = object;
        originName = auto1.city;
        destinationName = auto2.city;
        coordList = new ArrayList();
        coordList.add(auto1.lattidude);
        coordList.add(auto1.longitude);
        coordList.add(auto2.lattidude);
        coordList.add(auto2.longitude);
        drawColor = "red";
    }
}

class secondClass extends PacketClass{

    public secondClass(PacketObject object, SmartPostAutomat auto1, SmartPostAutomat auto2){
        classType = 2;
        deliveryObject = object;
        originName = auto1.city;
        destinationName = auto2.city;
        coordList = new ArrayList();
        coordList.add(auto1.lattidude);
        coordList.add(auto1.longitude);
        coordList.add(auto2.lattidude);
        coordList.add(auto2.longitude);
        drawColor = "green";
    }
}

class thirdClass extends PacketClass{

    public thirdClass(PacketObject object, SmartPostAutomat auto1, SmartPostAutomat auto2){
        classType = 3;
        deliveryObject = object;
        originName = auto1.city;
        destinationName = auto2.city;
        coordList = new ArrayList();
        coordList.add(auto1.lattidude);
        coordList.add(auto1.longitude);
        coordList.add(auto2.lattidude);
        coordList.add(auto2.longitude);
        drawColor = "blue";
    }
}