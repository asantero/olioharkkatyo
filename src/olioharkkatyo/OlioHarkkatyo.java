
package olioharkkatyo;


import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Antti
 */

//the main class for javafx that netbeans creates
public class OlioHarkkatyo extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }
    
    //overrides the default stop-method so we can save our log to file when the program is closed
    @Override
    public void stop(){
        System.out.println("Ohjelma sulkeutuu.");
        // Save file
        try {
            //create a new writer that "should" encode with utf-8 so we can get the umlauts properly
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Packetlog.txt"),StandardCharsets.UTF_8));
            //loop through the logkeeper and write them to file
            for(int i = 0; i < PacketLogKeeper.getInstance().size; i++){
                out.write(PacketLogKeeper.getInstance().logArr.get(i));
            }
            //print to console the path to written file and the filename and close writer
            System.out.println("Lokitiedosto lähetetyistä paketeista kirjoitettu kansioon " + System.getProperty("user.dir") + " tiedostonimellä Packetlog.txt");
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
