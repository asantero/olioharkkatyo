
package olioharkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Antti
 */

//Storage that keeps track of created packets. Once again similar to AutomatTracker and other trackers
public class PacketStorage {
    protected static ArrayList<PacketClass> StorageArr;
    private static PacketStorage instance = null;

    protected static int size;
    
    public PacketStorage(){
        StorageArr = new ArrayList();
        size = 0;
    }
    public static PacketStorage getInstance() {
        if(instance == null) {
            instance = new PacketStorage();
        }
        return instance;
    }
    public static void addPacket(PacketClass packet){
        StorageArr.add(packet);
        size++;
    }
    

}
