
package olioharkkatyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Antti
 */
//controller for the packetcreation window
public class PakettiFXMLController implements Initializable {

    @FXML
    private ComboBox<PacketObject> objectBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private ComboBox<SmartPostAutomat> departureBox;
    @FXML
    private ComboBox<SmartPostAutomat> destinationBox;
    @FXML
    private Button cancelButton;
    @FXML
    private Button creationButton;
    @FXML
    private ToggleGroup radioGroup;
    @FXML
    private RadioButton rb1;
    @FXML
    private RadioButton rb2;
    @FXML
    private RadioButton rb3;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //set values for radiobuttons
        rb1.setUserData(1);
        rb2.setUserData(2);
        rb3.setUserData(3);
        //populate comboboxes by looping through trackerobjects
        for(int i = 0; i < AutomatTracker.getInstance().size; i++){
            departureBox.getItems().add(AutomatTracker.autoArr.get(i));
            destinationBox.getItems().add(AutomatTracker.autoArr.get(i));
        }
        for(int i = 0; i < ObjectTracker.size; i++){
            objectBox.getItems().add(ObjectTracker.objectArr.get(i));
        }
    }    

    @FXML
    private void cancelPackageCreation(ActionEvent event) {
        //set the value to false so we know no new packet was created
        PackageCreationChecker.getInstance().setStatus(false);
        //close the window
        closeWindow(cancelButton);
    }

    @FXML
    private void finishPackageCreation(ActionEvent event) {
        //get the origin and destinationautomats from comboboxes
        //forgot to add a handler if these are not set. Can't be bothered now
        SmartPostAutomat buff1 = departureBox.getValue();
        SmartPostAutomat buff2 = destinationBox.getValue();
        //get the value from selected radiobutton so we know what class of packet to create
        int classT = (Integer)radioGroup.getSelectedToggle().getUserData();
        //check if a predefined object is selected in combobox
        if(objectBox.getValue() != null){
            //create a switch case on the classT value and use it to determine what classtype of packet to create
            switch(classT){
                        //create a new packetobject and add it straight to packetstorage
                case 1: PacketStorage.getInstance().addPacket(new firstClass(objectBox.getValue(), buff1, buff2));
                        break;
                case 2: PacketStorage.getInstance().addPacket(new secondClass(objectBox.getValue(), buff1, buff2));
                        break;
                case 3: PacketStorage.getInstance().addPacket(new thirdClass(objectBox.getValue(), buff1, buff2));
                        break;
            }
        }else{
                //check if new object is fragile
            boolean frag = fragileCheck.selectedProperty().getValue();
                //create a new object based on the values submitted to textfields and add it straight to objectTracker
                //again no error checking due to lack of time
            ObjectTracker.getInstance().addObject(new PacketObject(Integer.parseInt(sizeField.getText()), Integer.parseInt(massField.getText()), nameField.getText(), frag));
            //similar switchcase as above but access the last object in the objecttracker(the one we just created) when creating new packet
            switch(classT){
                case 1: PacketStorage.getInstance().addPacket(new firstClass(ObjectTracker.getInstance().objectArr.get(ObjectTracker.getInstance().size - 1), buff1, buff2));
                        break;
                case 2: PacketStorage.getInstance().addPacket(new secondClass(ObjectTracker.getInstance().objectArr.get(ObjectTracker.getInstance().size - 1), buff1, buff2));
                        break;
                case 3: PacketStorage.getInstance().addPacket(new thirdClass(ObjectTracker.getInstance().objectArr.get(ObjectTracker.getInstance().size - 1), buff1, buff2));
                        break;
            }     
        }
        //set the status that we succesfully created a new package and close the window
        PackageCreationChecker.getInstance().setStatus(true);
        closeWindow(creationButton);
    }
    public void closeWindow(Button button) {
        Stage scene = (Stage) button.getScene().getWindow();
        scene.close();
    }
}
