
package olioharkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Antti
 */

//Object that keeps track of the 'objects' you can put in packets. Works almost identical to automatTracker
public class ObjectTracker {
    public static ArrayList<PacketObject> objectArr;
    protected static int size;
    private static ObjectTracker instance = null;

    public ObjectTracker() {
        objectArr = new ArrayList();
        size = 0;
    }
    public static ObjectTracker getInstance() {
        if(instance == null) {
            instance = new ObjectTracker();
        }
        return instance;
    }
    public static void addObject(PacketObject obj){
        objectArr.add(obj);
        size++;
    }
            
    
}
