
package olioharkkatyo;

/**
 *
 * @author Antti
 */
    //Simple object for checking if a new packet was created
public class PackageCreationChecker {
    private static PackageCreationChecker instance = null;
    protected static boolean success;
    
    public PackageCreationChecker(){
        
    }
    
    public static PackageCreationChecker getInstance() {
        if(instance == null) {
            instance = new PackageCreationChecker();
        }
        return instance;
    }
    
    public static void setStatus(boolean i){
        success = i;
    }
    
    public static boolean getStatus(){
        return success;
    }
    
}
